import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import NavHeader from "../../components/NavHeader/NavHeader";
import { DOMAIN, TOKEN_CYBERSOFT } from "../../configURL/constant";

export default function ChiTietPhim() {
  let { id } = useParams();

  const [dataPhim, setDataPhim] = useState({});
  console.log(id);

  useEffect(() => {
    axios({
      url: DOMAIN + `/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    })
      .then((res) => {
        console.log(res);

        setDataPhim(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <div className="container p-5">
        <p>Tên phim: {dataPhim.tenPhim}</p>
      </div>
    </div>
  );
}

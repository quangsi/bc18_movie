import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getProfileUser } from "../../redux/action/userAction";
import { SET_PROFILE_USER } from "../../redux/constant/userConstant";

export default function ProfileUser() {
  let dispatch = useDispatch();
  useEffect(() => {
    // first
    // return () => {
    //   second
    // }

    dispatch(getProfileUser());
  }, []);

  return <div>ProfileUser</div>;
}

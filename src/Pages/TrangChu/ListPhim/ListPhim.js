import React from "react";
import { useSelector } from "react-redux";
import ItemPhim from "../ItemPhim/ItemPhim";

export default function ListPhim() {
  let { dsPhim } = useSelector((state) => state.phimReducer);

  console.log({ dsPhim });
  return (
    <div className="container mx-auto mt-20">
      <div style={{ flexWrap: "wrap" }} className="flex flex-wrap">
        {dsPhim.map((item) => {
          return <ItemPhim data={item} />;
        })}
      </div>
    </div>
  );
}

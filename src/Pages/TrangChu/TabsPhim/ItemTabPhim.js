import moment from "moment";
import React from "react";

export default function ItemTabPhim({ phim }) {
  return (
    <div className="flex w-full h-32 space-x-3">
      <img
        src={phim.hinhAnh}
        alt=""
        className="w-1/5 h-full flex-shrink-0 object-cover rounded"
      />

      <div className=" flex-grow ">
        <p className="text-red-500 font-medium">{phim.tenPhim}</p>

        <div className="grid  grid-cols-3 gap-4">
          {phim.lstLichChieuTheoPhim.map((item, indexItem) => {
            return (
              indexItem < 6 && (
                <button className="bg-gray-200 rounded cursor-pointer ">
                  {moment(item.ngayChieuGioChieu).format("DD-MM-YYYY")}
                </button>
              )
            );
          })}
        </div>
      </div>
    </div>
  );
}

import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { DOMAIN, TOKEN_CYBERSOFT } from "../../../configURL/constant";
import axios from "axios";
import "./tabPhim.css";
import ItemTabPhim from "./ItemTabPhim";
const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}
export default function TabsPhim() {
  const [dataRapPhim, setDataRapPhim] = useState([]);

  useEffect(() => {
    axios({
      url: DOMAIN + "/api/QuanLyRap/LayThongTinLichChieuHeThongRap",
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    })
      .then((res) => {
        console.log(res);
        setDataRapPhim(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(dataRapPhim);

  const renderCumRap = (danhSachLoaiRap) => {
    return danhSachLoaiRap.map((cumRap, index) => {
      return (
        <TabPane
          tab={<img className="w-14 h-14" src={cumRap.logo} />}
          key={index}
        >
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={callback}
          >
            {cumRap.lstCumRap.map((rap, index) => {
              return (
                <TabPane tab={<p className="">{rap.tenCumRap}</p>} key={index}>
                  <div
                    style={{ height: 500 }}
                    className="w-full space-y-3 overflow-y-scroll"
                  >
                    {rap.danhSachPhim.map((phim, indexPhim) => {
                      return indexPhim < 10 ? (
                        <ItemTabPhim phim={phim} />
                      ) : (
                        <></>
                      );
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  return (
    <div className="container mx-auto py-20">
      <Tabs
        tabPosition="left"
        defaultActiveKey="1"
        onChange={callback}
        style={{ height: 500 }}
      >
        {renderCumRap(dataRapPhim)}
      </Tabs>
    </div>
  );
}

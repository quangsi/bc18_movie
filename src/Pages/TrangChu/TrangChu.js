import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import NavHeader from "../../components/NavHeader/NavHeader";
import { DOMAIN, TOKEN_CYBERSOFT } from "../../configURL/constant";
import DesktopResponsive from "../../HOC/DesktopResponsive";
import { getPhimAction } from "../../redux/action/phimAction";
import ListPhim from "./ListPhim/ListPhim";
import TabsPhim from "./TabsPhim/TabsPhim";
export default function TrangChu() {
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getPhimAction());
  }, []);

  return (
    <div>
      <ListPhim />
      <TabsPhim />
    </div>
  );
}
// 15 *5 =75
// 1

import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";

const { Meta } = Card;
export default function ItemPhim({ data }) {
  console.log(data);
  return (
    <div className="w-1/3 p-3">
      <NavLink
        to={`/detail/${data.maPhim}`}
        className="block w-full shadow cursor-pointer"
      >
        <Card
          hoverable
          style={{ width: "100%" }}
          cover={
            <img
              className=" w-full h-32 object-cover "
              alt="example"
              src={data.hinhAnh}
            />
          }
        >
          <Meta
            title={
              <p className="text-xs text-center text-red-600">
                {data.tenPhim.length > 20
                  ? data.tenPhim.slice(0, 24) + "..."
                  : data.tenPhim}
              </p>
            }
            description="www.instagram.com"
          />
        </Card>
      </NavLink>
    </div>
  );
}

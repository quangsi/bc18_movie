import React from "react";

export default function NotFound() {
  return (
    <div className="bg-white flex flex-col w-screen h-screen justify-center items-center space-y-5">
      <h2 className="text-5xl">404</h2>

      <p className=" text-2xl text-red-500">
        Xin lỗi, trang bạn đang tìm kiếm không tồn tại!
      </p>
    </div>
  );
}

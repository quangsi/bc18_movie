import { Switch } from "react-router-dom";
import { Route } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import SpinnerLoading from "./components/SpinnerLoading/SpinnerLoading";
import ChiTietPhim from "./Pages/ChiTietPhim/ChiTietPhim";
import DangKi from "./Pages/DangKi/DangKi";
import DangNhap from "./Pages/DangNhap/DangNhap";
import NotFound from "./Pages/NotFound/NotFound";
import ProfileUser from "./Pages/ProfileUser/ProfileUser";
import TrangChu from "./Pages/TrangChu/TrangChu";
import Layout from "./Template/Layout";

function App() {
  return (
    <>
      <SpinnerLoading />
      <BrowserRouter>
        <Switch>
          <Route
            path="/"
            exact
            //  component={TrangChu}
            render={() => {
              return <Layout Component={TrangChu} />;
            }}
          />

          <Route
            path="/profile"
            exact
            //  component={TrangChu}
            render={() => {
              return <Layout Component={ProfileUser} />;
            }}
          />
          <Route path="/login" component={DangNhap} />
          <Route path="/register" component={DangKi} />
          <Route
            path="/detail/:id"
            // component={ChiTietPhim}
            render={() => {
              return <Layout Component={ChiTietPhim} />;
            }}
          />

          <Route path="*" component={NotFound} />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;

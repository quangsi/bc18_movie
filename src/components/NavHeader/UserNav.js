import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { SET_USER_INFOR } from "../../redux/constant/userConstant";
import { localServ } from "../../services/LocalServ";

export default function UserNav() {
  let userInfor = useSelector((state) => state.userReducer.userInfor);
  console.log({ userInfor });
  let dispatch = useDispatch();
  const handleLogout = () => {
    localServ.removeUserInfor();
    dispatch({
      type: SET_USER_INFOR,
      payload: null,
    });
  };
  return userInfor ? (
    <div className="space-x-3">
      <span>{userInfor.hoTen}</span>{" "}
      <button
        onClick={handleLogout}
        className="px-3 py-2 bg-red-600 text-white rounded cursor-pointer"
      >
        Đăng xuất{" "}
      </button>{" "}
    </div>
  ) : (
    <div className="space-x-3">
      {" "}
      <NavLink to="/login">
        <button className="px-3 py-2 bg-blue-600 text-white rounded cursor-pointer">
          {" "}
          Đăng nhập
        </button>
      </NavLink>
      <NavLink to="/rerister">
        <button className="px-3 py-2 bg-red-600 text-white rounded cursor-pointer">
          Đăng kí
        </button>
      </NavLink>
    </div>
  );
}

import {
  SET_SPINNER_END,
  SET_SPINNER_START,
} from "../constant/spinnerConstant";

let initialState = {
  isLoading: false,
  count: 0,
};

export const spinnerReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SPINNER_START: {
      state.count++;

      state.isLoading = true;

      return { ...state };
    }
    case SET_SPINNER_END: {
      state.count--;

      if (state.count === 0) {
        state.isLoading = false;
      }

      return { ...state };
    }
    default:
      return { ...state };
  }
};
// 1 api

// 3 api

// 1 bật tắt  count = 0 = > tắt
// 2 bật tắt
// 3

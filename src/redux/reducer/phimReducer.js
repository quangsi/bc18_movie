import { SET_DANH_SACH_PHIM } from "../constant/phimConstant";

let initialState = {
  dsPhim: [],
};

export const phimReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DANH_SACH_PHIM: {
      state.dsPhim = action.payload;
      return { ...state };
    }
    default:
      return { ...state };
  }
};

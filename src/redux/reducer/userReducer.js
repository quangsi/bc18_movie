import { localServ } from "../../services/LocalServ";
import localStorageServ from "../../serviceWorker/locaStorage.service";
import { SET_PROFILE_USER, SET_USER_INFOR } from "../constant/userConstant";

let initialState = {
  userInfor: localStorageServ.userInfor.get(),

  profileUser: null,
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_INFOR: {
      state.userInfor = action.payload;
      return { ...state };
    }
    case SET_PROFILE_USER: {
      state.profileUser = action.payload;

      return { ...state };
    }

    default:
      return { ...state };
  }
};

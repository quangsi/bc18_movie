import { combineReducers } from "redux";
import { phimReducer } from "./phimReducer";
import { spinnerReducer } from "./spinnerReducer";
import { userReducer } from "./userReducer";

export const rootReducer = combineReducers({
  userReducer,
  phimReducer,
  spinnerReducer,
});

import axios from "axios";
import { DOMAIN, TOKEN_CYBERSOFT } from "../../configURL/constant";
import httpServ from "../../serviceWorker/http.service";
import { SET_DANH_SACH_PHIM } from "../constant/phimConstant";

export const getPhimAction = () => {
  return (dispatch) => {
    // axios({
    //   url: DOMAIN + "/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01",
    //   method: "GET",
    //   headers: {
    //     TokenCybersoft: TOKEN_CYBERSOFT,
    //   },
    // })
    httpServ
      .layDanhSachPhim()
      .then((res) => {
        console.log("res service", res);
        dispatch({
          type: SET_DANH_SACH_PHIM,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

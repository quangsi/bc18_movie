import httpServ from "../../serviceWorker/http.service";
import localStorageServ from "../../serviceWorker/locaStorage.service";
import { SET_PROFILE_USER } from "../constant/userConstant";

export const getProfileUser = () => {
  return (dispatch) => {
    httpServ
      .layThongTinNguoiDung({
        taiKhoan: localStorageServ.userInfor.get().taiKhoan,
      })
      .then((res) => {
        dispatch({
          type: SET_PROFILE_USER,

          payload: res.data.content,
        });
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
